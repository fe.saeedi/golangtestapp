package main

import (
	"testing"
)
var urlManager = UrlManager{}
var urlConv = MyUrlConverter{}
func TestSetUrlValid(t *testing.T){
	longUrl :="https://gobyexample.com/url-parsing"
	key,err1 := urlManager.SetUrl(longUrl, urlConv)
	if err1 !=nil || key==""{
		t.Fatalf("fail for url:%v", longUrl)
	}
}
// not valid
func TestSetUrlSameCodForSameUrl(t *testing.T){
	longUrl :="https://gobyexample.com/url-parsing"
	key1,_ := urlManager.SetUrl(longUrl, urlConv)
	key2,_ := urlManager.SetUrl(longUrl, urlConv)
	if key1!=key2{
		t.Fatalf("keys are not same when url is exist , %v", longUrl)
	}
}
//  url is shor so err must be InvalidArgument
func TestSetUrlForShortUrl(t *testing.T){
	url :="https"
	_,err := urlManager.SetUrl(url, urlConv)

	if  _, ok := err.(InvalidArgument); !ok{
		t.Fatalf("fail for url:%v", url)
	}
	if err==nil{
		t.Fatalf("fail for url:%v", url)
	}

}


func TestGetUrl(t *testing.T) {
	longUrl :="https://gobyexample.com/url-parsing"
	key,_ := urlManager.SetUrl(longUrl, urlConv)
	val, ok := urlManager.Get(key)
	if val!=longUrl || ok!=true {
		t.Fatalf("Url dose not get: %v", longUrl)
	}
}
