package main

import (
	"fmt"
)

type InvalidArgument string

func (ia InvalidArgument)  Error()string{
	return fmt.Sprintf("not valid arguments : %v", ia)

}