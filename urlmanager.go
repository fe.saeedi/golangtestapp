package main

import (
	"strconv"

	log "github.com/sirupsen/logrus"
)

type UrlManager struct {
	UrlCaches map[int64]string
}

//get url and add to dic and return key
//steps:
//1:validate url
//:is url repeat? if no: generate key and add to cache

func (m *UrlManager) SetUrl(url string, urlHelper UrlConverter) (string, error) {
	if m.UrlCaches == nil {
		m.UrlCaches = make(map[int64]string)
	}
	if ok := urlHelper.IsValid(url) ; !ok{
		return "",InvalidArgument(url)
	}
	newKey := urlHelper.Encode(url)
	_, ok := m.UrlCaches[newKey]
	if !ok{
		m.UrlCaches[newKey] = url
	}
	return strconv.FormatInt(newKey, 10), nil

}

// get long url with code
// if code is not int then error
func (m UrlManager) Get(code string) (string, bool) {
	key,err := strconv.ParseInt(code, 0, 64)
	log.WithFields(log.Fields{
		"key": key,
		"cach":m.UrlCaches,
	}).Info("UrlManager.Get")
	if err !=nil {
		return "", false
	}
	url, ok :=m.UrlCaches[key]
	return url, ok
}
