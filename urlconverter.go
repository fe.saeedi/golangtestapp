package main

type UrlConverter interface {
	Encode(url string ) int64
	IsValid( url string) bool
}