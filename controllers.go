package main

import (
	"net/http"

	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

var urlManager = UrlManager{}
var urlConv = MyUrlConverter{}
const prefixCode = "http://myhost.com/"

func handelGet(c echo.Context) error {
	code := c.Param("code")

	res, ok := urlManager.Get(code)
	if ok{
		return c.String(http.StatusOK, res)
	}else{
		return c.String(http.StatusBadRequest, "")
	}


}

func handelPost(c echo.Context) error {
	longurl := c.FormValue("path")
	res, err := urlManager.SetUrl(longurl, urlConv)
	if err == nil {
		return c.String(http.StatusOK, prefixCode + res)
	} else {
		if _, ok := err.(InvalidArgument); ok {
			return c.String(http.StatusBadRequest, "")
		} else {
			return c.String(http.StatusInternalServerError, "")
		}
	}

}

func apiWrap(f func (echo.Context) error)func (echo.Context) error{
	return func(c echo.Context) error {
		logContext(c)
		err := f(c)
		log.Info("finish call api")
		return err
	}
}

func logContext(c echo.Context){
	paramNAmes := c.ParamNames()
	strPAram :=""
	for _,key :=range paramNAmes{
		strPAram = strPAram + "," +key +":"+ c.Param(key)
	}
	forms,_ := c.FormParams()
	queries := c.QueryParams()
	log.WithFields(log.Fields{
		"path":c.Path(),
		"forms":forms,
		"params":strPAram,
		"queries":queries,
	}).Info("start call api")
}

func main() {
	log.Info("Application start")
	e := echo.New()
	e.GET("/api/v1/urls/:code", apiWrap(handelGet))
	e.POST("/api/v1/urls", apiWrap(handelPost))
	e.Logger.Fatal(e.Start(":1323"))
}
