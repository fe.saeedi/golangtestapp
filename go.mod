module golangtestapp

go 1.13

require (
	github.com/labstack/echo/v4 v4.6.1
	github.com/sirupsen/logrus v1.8.1
)
