package main

import (
	"testing"
	"time"
)

var urlconv = MyUrlConverter{}

func TestEncode(t *testing.T) {
	url := "https.google.com"
	wantCodeMin := time.Now().Unix()
	wantCodeMax := time.Now().Add(time.Minute).Unix()
	res := urlconv.Encode(url)
	if res < wantCodeMin || res> wantCodeMax{
	t.Fatalf("fail for encode url, your url is:%v, wantCodeMin url is :%v, wantCodeMax url is :%v, res is : %v", url, wantCodeMin, wantCodeMax, res)
	}
}


func TestIsValid(t *testing.T){
	items :=[]struct {
		UrlInput string
		IsValid bool
	}{
		{UrlInput : "https://gobyexample.com/url-parsing",IsValid:true},
		{UrlInput : "https://gobyexample.com/",IsValid:false},
		{UrlInput : "hghhhhhhhhh",IsValid:false},
	}
	for _,item := range items{
		res := urlconv.IsValid(item.UrlInput)
		if item.IsValid != res{
			t.Fatalf("fail for valid url, your url is:%v, want res is :%v, res is : %v", item.UrlInput, item.IsValid, res)
		}
	}
}

